const http = require("http");

const port = 3000;

const server = http.createServer((req, res)=>{
	if (req.url === "/items" && req.method === "POST") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database.");
	}
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);