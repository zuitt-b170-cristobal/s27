const http = require("http");

//Mock Database
let database = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"

}
]

http.createServer((req, res) =>{
	//route for returning all items upon receiving a get req
	if (req.url === "/users" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "_application/json"});
		//res.write() func is for printing whats inside the parameters as a response
		//input must be string so JSON.stringify is used.
		//data to be received by users/client from the server will be in a form of stringified JSON
		res.write(JSON.stringify(database));
		res.end();
	}
	if (req.url === "/users" && req.method === "POST") {
		let requestBody = ""
		/*
		data stream - flow/sequence of data

		 data step - data is received from client & processed in stream called "data" where the code/statements will be triggered. 

		 end step - only runs after the request has completely been sent once the data has already been processed
		*/
		req.on("data", function(data){
			//data will be assigned as the value of requestBody
			requestBody += data
		})
		req.on("end", function() {
			//server needs object to arrange info in database so use JSON.parse
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			//adds newUser into the database
			database.push(newUser);
			console.log(database);

			res.writeHead(200, {"Content-Type": "_application/json"});
			//client side needs string data for readability, so use JSON.stringify
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}


}).listen(4000)

console.log("Server is running at localhost: 4000")

/*
save function instead of push

*/