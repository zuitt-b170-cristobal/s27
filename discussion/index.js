const http = require("http");

const port = 4000;

const server = http.createServer((req, res)=>{
	//http method of incoming requests can be accessed via req.method
		//GET method - responsible for retrieving/reading info; default method
	/*
	POST, PUT, DELETE operation do not work in the browser unlike the Get method. w/ this, Postman solves the problem by stimulatinga frontend for the devs to test their codes.
	*/
	if (req.url === "/items" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data retrieved from database.");
	}
	if (req.url === "/items" && req.method === "POST") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database.");
	}
	if (req.url === "/items" && req.method === "PUT") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database.");
	}
	if (req.url === "/items" && req.method === "DELETE") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database.");
	}
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);

